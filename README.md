Merlin Filebeat Module
======================

Filebeat configuration files and custom module for merlin router firmware
system log processing.

Setup:

* properly configured elasticsearch and kibana (ek) cluster
* router configured to forward system logs to local host
* filebeat.yml updated with ek cluster's ip address and login info

Assumptions:

* router system log files stored at ../logs/router.log
